# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://binei@bitbucket.org/binei/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/binei/stroboskop/commits/5176ec3695c4f733d78e52976452ea4589fed091

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/binei/stroboskop/commits/d9cf3ee9477505cb451a880cb15d420751b324a1

Naloga 6.3.2:
https://bitbucket.org/binei/stroboskop/commits/c18c1fab7ad1077a361af028a88ed52e506813fc

Naloga 6.3.3:
https://bitbucket.org/binei/stroboskop/commits/644e69865eb516428348ad9dd8fe6bef7166cb23

Naloga 6.3.4:
https://bitbucket.org/binei/stroboskop/commits/401a041a29fa28b750628562d42a508782a5b29f

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/binei/stroboskop/commits/4d339ff46096a0f8fdc0aaf51ed0a87f1d804202

Naloga 6.4.2:
https://bitbucket.org/binei/stroboskop/commits/90fb79783938f6a544cb7de5c8af78bace513e34

Naloga 6.4.3:
https://bitbucket.org/binei/stroboskop/commits/7cc5007cc188edb55ca5ca96d1de7fefb3990648

Naloga 6.4.4:
https://bitbucket.org/binei/stroboskop/commits/be0eeba806a07e6a10503b4f42917e27986699be